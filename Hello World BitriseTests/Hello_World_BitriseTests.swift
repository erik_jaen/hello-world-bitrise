//
//  Hello_World_BitriseTests.swift
//  Hello World BitriseTests
//
//  Created by Erik Jaen on 05.02.19.
//  Copyright © 2019 Erik Jaen. All rights reserved.
//

import XCTest
@testable import Hello_World_Bitrise

class Hello_World_BitriseTests: XCTestCase {

    let A = 10, B = 5
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    func testAdd() {
        XCTAssertEqual(A + B, 15)
    }

    func testSubtract() {
        XCTAssertEqual(A - B, 5)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
